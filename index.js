/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function


*/



function Pokemon(name,level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target){
		console.log(this.name + " tackled" + " " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health = (target.health - this.attack)
		if(target.health <5){
			this.faint(target.name)
		}

	},
	this.faint = function(targetpokemon){
			console.log(targetpokemon + " has fainted")}
		
}

let pikachu = new Pokemon ("Pikachu", 16)
let squirtle = new Pokemon ("Squirtle", 8)
let charmander = new Pokemon ("Charmander", 18)
console.log(pikachu)
console.log(squirtle)
console.log(charmander)

pikachu.tackle(squirtle)
pikachu.tackle(squirtle)
pikachu.tackle(charmander)
pikachu.tackle(charmander)
pikachu.tackle(charmander)
pikachu.tackle(charmander)
squirtle.tackle(pikachu)
squirtle.tackle(pikachu)
squirtle.tackle(pikachu)
squirtle.tackle(pikachu)
squirtle.tackle(pikachu)
squirtle.tackle(pikachu)